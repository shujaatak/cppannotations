The next listing (described below the listing) defines a complete module:
    verbinclude(-ans4 examples/complete/complete.cc)

This initial module is not exactly a thing of beauty, but we must start
somewhere:
    itemization(
    it() Line 1: hi(module: export) modules start with tt(export module)
        followed by the module's name. Names are identifiers, possibly
        preceded by series of `tt(identifier .)' or `tt(identifier :)'
        sequences. The former syntax merely defines a module's name (like
        tt(this.is.my.module)), the latter is used for em(partitions), covered
        in section ref(PARTITIONS)

    it() At lines 3, 5, 19 and 24 a variable, a common function and two class
        members are defined. There's nothing special here: just
        implementations.

    it() In lines 10 through 17 the interface of the class tt(Class) is
        provided. Again: nothing special.
    )

A sourcce file starts a module definition at its tt(export module)
line. Everything from that line onward is part of the module, so one source
file cannot define multiple modules. Everything below that line defines the
components of that module. In this case a variable, a function, a class
interface and two member function definitions.

Assuming this module is implemented in tt(complete.cc) then
        verb(
    g++ -c --std=c++20 -fmodules-ts -Wall complete.cc
        )
    the following file system elements are available:
    itemization(
    itt(complete.o,) the compiled module
    itt(gcm.cache/complete.gcm,) the module's interface.nl()
        the interface resembles a precompiled header, but its usually much
        smaller.
    )

However, in line with the `factory' metaphor used in the previous section,
nothing so far is accessible from outside of the module. So a member like
tt(Class::value), being a component of tt(complete), can use tt(counter) and
call tt(square), but no other software can.

To make components visible to the outside world they must explicitly be
em(exported). To export types (and by implication: their elements) prefix their
type declaration by tt(export), as in tt(export class Class). To export
variables prefix their definitions by tt(export) and to export functions
prefix their function heads by tt(export):
        verb(
    export unsigned counter = 0;
    
    export double square(double value)
    {
        return value * value;
    }
        )

