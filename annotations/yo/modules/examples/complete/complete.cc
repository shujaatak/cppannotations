export module complete;

// modules cannot be embedded in namespaces, but namespaces can be
// embedded in modules. Also, the module.gcm cache files are directly
// connected to the module.cc source file, and cannot be renamed.
// The module name can freely be chosen, but somehow collisions of filenames
// defining modules must be prevented.

namespace FBB
{
    export unsigned counter = 0;
}

export double square(double value)
{
    return value * value;
}

class Class
{
    int d_value;

    public:
        Class(int value);
        int value() const;
};

Class::Class(int value)
:
    d_value(value)
{}

int Class::value() const
{
    return d_value;
}
