#ifndef INCLUDED_DONE_
#define INCLUDED_DONE_

#include "../../promisebase/promisebase.h"
#include "../awaiter/awaiter.h"

class Done: public Awaiter
{
    struct State: public PromiseBase<Done, State>
    {};

    std::coroutine_handle<State> d_handle;
    static size_t s_count;

    public:
        typedef State promise_type;
        typedef std::coroutine_handle<State> Handle;

        explicit Done(Handle handle);
        ~Done();

        template <typename HandleType>
        std::coroutine_handle<State> await_suspend(HandleType const &handle);
};

template <typename HandleType>
inline std::coroutine_handle<Done::State> Done::await_suspend(
                                              HandleType const &handle)
{
    return d_handle;
}

extern Done g_done;

#endif
