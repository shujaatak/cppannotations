#ifndef INCLUDED_LETTER_
#define INCLUDED_LETTER_

#include "../../promisebase/promisebase.h"

class Letter
{
    struct State: public PromiseBase<Letter, State>
    {};

    std::coroutine_handle<State> d_handle;

    public:
        typedef State promise_type;
        typedef std::coroutine_handle<State> Handle;

        explicit Letter(Handle handle);
        ~Letter();

        Handle handle() const;
};

inline Letter::Handle Letter::handle() const
{
    return d_handle;
}

extern Letter g_letter;

#endif
