#ifndef INCLUDED_DONE_
#define INCLUDED_DONE_

#include "../../promisebase/promisebase.h"

class Done
{
    struct State: public PromiseBase<Done, State>
    {};

    std::coroutine_handle<State> d_handle;
    static size_t s_count;

    public:
        typedef State promise_type;
        typedef std::coroutine_handle<State> Handle;

        explicit Done(Handle handle);
        ~Done();

        Handle handle() const;
};

inline Done::Handle Done::handle() const
{
    return d_handle;
}

extern Done g_done;

#endif
