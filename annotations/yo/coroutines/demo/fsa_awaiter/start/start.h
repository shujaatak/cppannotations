#ifndef INCLUDED_START_
#define INCLUDED_START_

#include "../../promisebase/promisebase.h"

// no operator co_await, separate Awaiter

//start
class Start
{
    struct State: public PromiseBase<Start, State>
    {};

    std::coroutine_handle<State> d_handle;

    public:
        typedef State promise_type;
        typedef std::coroutine_handle<State> Handle;

        explicit Start(Handle handle);
        ~Start();

        Handle handle() const;
        void go();
};

inline Start::Handle Start::handle() const
{
    return d_handle;
}

extern Start g_start;

#endif
