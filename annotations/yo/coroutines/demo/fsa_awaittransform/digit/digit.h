#ifndef INCLUDED_DIGIT_
#define INCLUDED_DIGIT_

#include "../../promisebase/promisebase.h"
#include "../awaiter/awaiter.h"

class Digit
{
    struct State: public PromiseBase<Digit, State>
    {
        template <typename Handler>
        Awaiter await_transform(Next<Handler> &&next);
    };

    std::coroutine_handle<State> d_handle;

    public:
        typedef State promise_type;
        typedef std::coroutine_handle<State> Handle;

        explicit Digit(Handle handle);
        ~Digit();

        Handle handle() const;
};

template <typename Handler>
Awaiter Digit::State::await_transform(Next<Handler> &&next)
{
    return Awaiter{ next.d_handle };
}

inline Digit::Handle Digit::handle() const
{
    return d_handle;
}

extern Digit g_digit;

#endif
