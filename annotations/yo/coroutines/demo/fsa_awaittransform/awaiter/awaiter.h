#ifndef INCLUDED_AWAITER_
#define INCLUDED_AWAITER_

#include <coroutine>

template <typename Handler>
struct Next
{
    std::coroutine_handle<> d_handle;
    
    Next(Handler &handler, char ch)
    :
        d_handle(handler.handle())
    {}

    Next(Handler &handler)
    :
        d_handle(handler.handle())
    {}
};


//class
class Awaiter
{
    typedef std::coroutine_handle<> Handle;

    Handle d_handle;

    public:
        Awaiter() = default;
        Awaiter(Handle handle);
    
        Handle await_suspend(Handle const &handle);
    
        static bool await_ready();
        static void await_resume();
};

inline Awaiter::Awaiter(Handle handle)
:
    d_handle(handle)
{}

inline  Awaiter::Handle Awaiter::await_suspend(Handle const &handle)
{
    return d_handle;
}

inline bool Awaiter::await_ready()
{
    return false;
}

inline void Awaiter::await_resume()
{}
//=

#endif
