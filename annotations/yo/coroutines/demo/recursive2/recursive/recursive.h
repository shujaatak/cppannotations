#ifndef INCLUDED_RECURSIVE_
#define INCLUDED_RECURSIVE_

#include "../../promisebase/promisebase.h"

class Recursive
{
    class State: public PromiseBase<Recursive, State>
    {
        typedef std::coroutine_handle<State> Handle;

        size_t d_value;

        public:
            std::suspend_always yield_value(size_t value);
            size_t value() const;
    };

    private:
        typedef std::coroutine_handle<State> Handle;
        Handle d_handle;

    public:
        typedef State promise_type;

        explicit Recursive(std::coroutine_handle<State> handle);
        ~Recursive();

        bool next(size_t *value);
};

inline Recursive::Recursive(std::coroutine_handle<State> handle)
:
    d_handle(handle)
{}

#endif
