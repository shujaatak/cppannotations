One important difference between function templates and class templates is
that the template arguments of function templates can normally be deduced by
the compiler, whereas template arguments of class templates may have to be 
specified by the user. That's not always required, but since the template
types of class templates are an inherent part of the class template's type it
must be completely clear to the compiler what the class template's types are.

So, in case we have a 
    verb(    template <typename T>
    void fun(T const &param);)

we can do
    verb(    vector<int> vi;
    fun(vi))

and the compiler deduces tt(T == vector<int>). 

On the other hand,  if we have
    verb(    template <typename T>
    struct Fun
    {
        T d_data;
        Fun();
    };)

we can+bf(not) do
    verb(    
    Fun fun;)

since tt(Fun) is not a type, and the compiler cannot deduce what the intended
type is.

Sometimes the compiler em(can) deduce the intended types. Consider this:
    verb(
    vector vi{ 1, 2, 3, 4 };)

In this case the compiler deduces tt(int) from the provided values. The
compiler is smart enought to select the most general type, as in this example,
where tt(double) is deduced:
    verb(
    vector vi{ 1, 2.5 };)

The compiler is doing its utmost to deduce types when they're not
explicitly specified, and it will stay on the safe side. So the vector is a
tt(vector<int>) in the first example and a tt(vector<double>) in the second. 

Although it's nice that the compiler is willing and able to deduce types in
many situations it's at the same time a potential source of confusion. In the
first example only non-negative values are used, so why not define a
tt(vector<unsigned>) or a tt(vector<size_t>)? In the second example the
compiler deduces tt(vector<double>), but tt(vector<float>) could very well
also have been used. 

To avoid confusion, as a i(rule of thumb), it might be a good idea to specify
the types of class templates when defining class type objects. It requires
hardly any effort and it completely clarifies what you want and mean. So
prefer
    verb(
    vector<int>     vi{ 1, 2, 3 };
    vector<double>  vd{ 1, 2.5 };)
    
over the definitions implicitly using types.
