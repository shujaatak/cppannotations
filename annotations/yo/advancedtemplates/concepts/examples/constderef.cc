template <typename Type>
concept ConstDereferenceable =
    requires(Type type)
    {
        { *type } -> std::same_as<typename Type::value_type const &>;
    };
