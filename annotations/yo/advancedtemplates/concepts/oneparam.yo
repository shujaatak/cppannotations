The following concepts specify just one template type parameter. Their generic
form is 
    verb(    template <typename Type>
    concept Name = 
        ... requirements ...
        ;)
    When used in compound requirements only their names have to be
specified. For example (using the concept tt(std::boolean) (see below)), to
require that a function tt(fun) receiving an argument of some type tt(Type)
returns a boolean value, the following concept could be defined:
        verb(template<typename Type>
    concept BoolFun = 
        requires(Type param)
        {
            { fun(param) } -> std::boolean;
        };)

itemization(
ittq(boolean)(requires that its type can be used in boolean expressions)

ittq(copy_constructible)(requires that objects of its type supports copy- and
    move-construction;)

ittq(copyable)(requires that objects of its type support copy- and move-
    construction and assignment, and that two objects of its type can be
    swapped;) 

ittq(default_initializable)(requires that objects of its type support default
    constructors;) 

ittq(destructible)(requires that the destructor of its type is defined as
    tt(noexcept(true));)

ittq(equality_comparable)(requires that tt(operator==) is available to compare
    two objects of its type;)

ittq(floating_point)(requires that its type is a floating-point type;)

ittq(integral)(requires that its type is an integral  type;)

ittq(movable)(requires that its type supports moving and swapping. Note that
    full support for moving requires that its type supports move construction
    and move assignment;)

ittq(move_constructible)(requires that its type supports move construction;)

ittq(regular)(requires that its type satisfies the requirements of the
    tt(semiregular) and tt(equality_comparable) concepts;)

ittq(semiregular)(requires that its type supports default construction,
    copying, moving, and swapping;)

ittq(signed_integral)(requires that its type is a signed integral type;)

ittq(swappable)(requires that two objects of the same type can be swapped. The
    generic variant (see below) is named tt(swappable_with);)

ittq(unsigned_integral)(requires that its type is an unsigned integral type;)

ittq(totally_ordered)(requires that two objects of identical
    types can be ordered using the operators tt(==, !=, <, <=, >,) and
    tt(>=). The requirements for ordering is hi(ordering: strict)em(strict):
    for any two objects tt(one) and tt(two) either tt(one < two, one == two,)
    or tt(one > two) is true. The generic variant is named
    tt(totally_ordered_with).)
)
